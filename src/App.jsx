import { useEffect, useRef, useState } from "react"
import { useReactToPrint } from 'react-to-print';
import { Icon } from '@iconify/react';
import { fileData } from "./data";

const App = () => {
  const pdfDiv = useRef()

  const [data, setData] = useState()
  const [language, setLanguage] = useState('english')

  const imgStyles = { backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundSize: 'cover' }

  const bg0 = { backgroundColor: '#312e81' }
  const color0 = { color: '#312e81' }
  const color1 = { color: '#f8fafc' }
  const color2 = { color: '#cbd5e1' }
  const color3 = { color: '#0f172a' }
  const color4 = { color: '#334155' }
  const color5 = { color: '#64748b' }

  const handlePrint = useReactToPrint({
    content: () => pdfDiv.current,
  });



  useEffect(() => {
    setData(fileData)
  }, [])

  return (
    <div className='w-screen h-screen bg-slate-400 flex justify-center overflow-y-auto'>
      <div className="absolute top-0 left-0 m-1 flex flex-col">
        <button onClick={handlePrint} className="rounded bg-indigo-500 p-2">save</button>

      </div>
      {!data ? null :
        <div id="pdf-page" ref={pdfDiv} className="bg-white relative">
          {/* left side */}
          <div className={`absolute w-80 h-full px-4 py-4 space-y-8`} style={bg0}>
            <h1 className="text-4xl leading-relaxed" style={color1}>{data.name || 'Nameless'}</h1>
            <div className="w-72 h-72 flex items-center justify-center rounded-full border-4 border-slate-400" style={{ ...imgStyles, ...{ backgroundImage: `url(${data.imageUrl})` } }}></div>

            <div className="space-y-2">
              <h2 style={color1}>About me</h2>
              <p className="leading-5" style={color2}>{data.description || 'Opis tutej.'}</p>
            </div>
            <div className="space-y-2">
              <h2 style={color1}>Contact</h2>
              <ul className="w-full space-y-1">
                {Object.keys(data.contact).map(i => (
                  <li key={i} className="flex space-y-1 space-x-2 items-center">
                    <Icon className="text-2xl" style={color1} icon={`mdi:${i}`} />
                    {['web', 'gitlab', 'github'].includes(i) ?
                      <a target='#' href={data.contact[i]} style={color2}>{data.contact[i].replace('https://', '')}</a>
                      :
                      <div>
                        {i === 'email' ?
                          <a href={`mailto:${data.contact[i]}`} style={color2}>{data.contact[i]}</a>
                          :
                          <span style={color2}>{data.contact[i]}</span>
                        }
                      </div>
                    }
                  </li>
                ))}
              </ul>
            </div>
            <div style={color1} className="space-y-2">
              <h2>Languages</h2>
              <ul className="space-y-1">
                <li>English: <span style={color2}>B2</span></li>
                <li>Polish: <span style={color2}>Native</span></li>
              </ul>
            </div>
          </div>
          {/* right side */}
          <div className="absolute w-fit h-full left-80 bg-slate-50 px-4 py-8 space-y-2">

            <section className="space-y-2">
              <div className="flex space-x-2 items-center" style={color0}>
                <Icon className="text-2xl" icon="material-symbols:done-all" />
                <h2 className="text-2xl"><span>Projects</span></h2>
              </div>
              <div className="pl-4">
                <ul className="space-y-2">
                  {data.projects.map((i, index) => (
                    <li key={index} className={`pb-2 space-y-1 ${index === data.projects.length - 1 ? '' : 'border-b'}`}>
                      <h1 className="text-lg" style={color3}>{i.name} </h1>
                      <div>
                        <a target='#' href={i.url} className='flex space-x-2 items-center' style={color4}>
                          <Icon className="text-xl" icon="mdi:web" />
                          <span>{i.url.replace('https://', '')}</span>
                        </a>
                        <a target='#' href={i.repository} className='flex space-x-2 items-center' style={color4}>
                          <Icon className="text-xl" icon="mdi:gitlab" />
                          <span>{i.repository.replace('https://', '')}</span>
                        </a>
                      </div>
                      <p className="text-sm text-slate-800">{i.description}</p>
                    </li>
                  ))}
                </ul>
              </div>
            </section>

            <section className="space-y-2">
              <div className="flex space-x-2 items-center" style={color0}>
                <Icon className="text-2xl" icon="ic:round-work" />
                <h2 className="text-2xl"><span>Experience</span></h2>
              </div>
              <div className="pl-4">
                <ul className="space-y-2">
                  {data.experience.map((i, index) => (
                    <li key={index} className={`pb-2 ${index === data.experience.length - 1 ? '' : 'border-b'}`}>
                      <h1 style={color3} className="text-lg">{i.title} | {i.employee}</h1>
                      <ul className="pl-5 list-disc">
                        {i.description.map(j => (
                          <li key={j} className="text-sm">
                            {j.startsWith('https://') ? <a target='#' href={j}>{j.replace('https://', '')}</a> : <span>{j}</span>}
                          </li>
                        ))}
                      </ul>
                      <p style={color5} className="text-xs leading-6">from {i.dateFrom} to {i.dateTo}</p>
                    </li>
                  ))}
                </ul>
              </div>
            </section>

            <section className="space-y-2">
              <div className="flex space-x-2 items-center" style={color0}>
                <Icon className="text-2xl" icon="mdi:university" />
                <h2 className="text-2xl"><span>Education</span></h2>
              </div>
              <div className="pl-4">
                <ul className="space-y-2">
                  {data.education.map((i, index) => (
                    <li key={index} className={`pb-2 ${index === data.education.length - 1 ? '' : 'border-b'}`}>
                      <h1 style={color3} className="text-lg">{i.title}</h1>
                      <p style={color4} className="text-sm">{i.place}</p>
                      <p style={color5} className="text-xs leading-6">from {i.dateFrom} to {i.dateTo}</p>
                    </li>
                  ))}
                </ul>
              </div>
            </section>

            <section className="space-y-2">
              <div className="flex space-x-2 items-center" style={color0}>
                <Icon className="text-2xl" icon="mdi:code-tags" />
                <h2 className="text-2xl"><span>Technologies</span></h2>
              </div>
              <div className="pl-4 flex flex-col">
                {Object.keys(data.skills).map(i => (
                  <div key={i}>
                    <h3 className="text-sm">{i}</h3>
                    <ul className="flex flex-wrap">
                      {data.skills[i].map(j => (
                        <li key={j} style={{ ...bg0, ...color1 }} className="rounded-xl border text-xs w-min p-2 flex items-center mr-1 mb-1">{j}</li>
                      ))}
                    </ul>
                  </div>
                ))}
              </div>
            </section>
            <p style={{ ...color5, ...{ fontSize: '.60rem' } }} className="absolute bottom-1">I consent to the processing of my personal data for the purpose of recruiting for the position I am applying.</p>
          </div>
        </div>
      }
    </div>
  )
}

export default App
